﻿using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    public static void Shuffle<T>(List<T> data)
    {
        var length = data.Count;
        
        for (var i = length - 1; i > 1; i--)
        {
            var randomIndex = Random.Range(0, i);
            var temp = data[i];
            data[i] = data[randomIndex];
            data[randomIndex] = temp;
        }
    }
}