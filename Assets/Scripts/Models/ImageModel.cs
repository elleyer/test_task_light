﻿using System.Collections.Generic;

namespace Models
{
    public class ImagesRoot
    {
        public List<ImageModel> Images { get; set; }
    }

    public class ImageModel
    {
        public string Link { get; set; }
        public string Hash { get; set; }
    }
}