﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Handlers
{
    public class SessionHandler : MonoBehaviour
    {
        private const string URI = "https://drive.google.com/uc?id=1cw6l5FEDcwWYwxAosk-yaEqHlQNq9Df8&export=download";
    
        private const int AMOUNT_OF_CARD_TYPES = 3;
        private const int PAIR_LENGTH = 2;
        private const int AMOUNT_OF_CARDS = AMOUNT_OF_CARD_TYPES * PAIR_LENGTH;

        [SerializeField] private UIHandler _uiHandler;
        [SerializeField] private Card _cardPrefab;
        [SerializeField] private NetworkHandler _networkHandler;
        [SerializeField] private Transform _parent;

        private List<Texture2D> _cachedTextures;
        private List<CardData> _cardsData;

        private Card[] _cards;
        private Card _prevCard;

        private int _score;
        private int _round;
        private int _pairsGone;

        private bool _isBusy;

        private void Start()
        {
            _cards = new Card[AMOUNT_OF_CARDS];

            for (var i = 0; i < AMOUNT_OF_CARDS; i++)
            {
                var card = Instantiate(_cardPrefab, _parent);
                card.OnCardSelected = CardSelected;
                card.name = $"Card {i}";
                _cards[i] = card;
            }

            _networkHandler.OnTextureLoadComplete = LoadCards;
            Init();
        }
    
        private void Init()
        {
            _round++;
            _uiHandler.SetRound(_round);

            if (_cachedTextures == null)
            {
                _networkHandler.LoadTextures(URI);
                return;
            }
        
            LoadCards(_cachedTextures);
        }
    
        private void ResetCards()
        {
            _cardsData.Clear();
            foreach (var card in _cards)
                card.Image.enabled = true;
        
            _uiHandler.SetGameState(false);
            Init();
        }

        private void ChangeSelectionAll(bool selected)
        {
            foreach (var card in _cards)
                card.ChangeSelection(selected);
        }

        private void InitializeCards(List<Texture2D> textures)
        {
            Utilities.Shuffle(textures);
            _cardsData = new List<CardData>();

            var currentTexture = 0;
        
            //Create 6 / 2 = 3 different pairs
            for (var i = 0; i < AMOUNT_OF_CARDS - 1; i += PAIR_LENGTH)
            {
                var texture = textures[currentTexture];
                _cardsData.Add(new CardData(i, texture));
                _cardsData.Add(new CardData(i, texture));
            
                currentTexture++;
            }
        
            Utilities.Shuffle(_cardsData);

            for (var i = 0; i < _cardsData.Count; i++)
                _cards[i].SetData(_cardsData[i]);
        
            //Show our cards at the beginning.
            ChangeSelectionAll(true);
            DOTween.Sequence().AppendInterval(5f).OnComplete(() => ChangeSelectionAll(false));
        }

        private void CardSelected(Card card)
        {
            if (_isBusy)
                return;
        
            card.ChangeSelection(true);
        
            //check if our prev. card exists and has the same Id
            if (_prevCard != null)
            {
                var sequence = DOTween.Sequence();
                sequence.AppendInterval(1f).OnComplete(() =>
                {
                    if (_prevCard.Id == card.Id)
                    {
                        _prevCard.Image.enabled = false;
                        card.Image.enabled = false;
                    
                        _score++;
                        _uiHandler.SetScore(_score);

                        _pairsGone++;
                        if (_pairsGone == AMOUNT_OF_CARD_TYPES)
                        {
                            _pairsGone = 0;
                            ResetCards();
                        }
                    }

                    else
                    {
                        _prevCard.ChangeSelection(false);
                        card.ChangeSelection(false);
                    }

                    _isBusy = false;
                    _prevCard = null;
                });

                _isBusy = true;
                sequence.Play();
                return;
            }

            _prevCard = card;
        }
    
        private void LoadCards(List<Texture2D> textures)
        {
            _cachedTextures ??= textures;
        
            _uiHandler.SetGameState(true);
            InitializeCards(textures);
        }
    }
}
