﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Card : MonoBehaviour, IPointerDownHandler
{
    //Set it's id in Init method.
    public int Id { get; set; }

    //Is our card currently selected?
    public bool Selected { get; private set; }

    public Action<Card> OnCardSelected;

    public Image Image;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Selected)
            return;
        
        OnCardSelected(this);
    }

    public void ChangeSelection(bool selected)
    {
        Selected = selected;
        FlipCard();
    }

    public void Awake()
    {
        Image = GetComponent<Image>();
        Image.material = new Material(Image.material);
    }

    public void SetData(CardData data)
    {
        Id = data.Id;
        Image.material.SetTexture("_FrontTex", data.Texture);
    }
    
    private void FlipCard()
    {
        var rot = transform.rotation;
        var rotY = Selected ? 0 : 180;
        
        transform.DORotate(new Vector3(rot.x, rotY, rot.z), 0.5f);
    }
}
