﻿using UnityEngine;

public struct CardData
{
    public int Id;
    public Texture2D Texture;

    public CardData(int id, Texture2D tex)
    {
        Id = id;
        Texture = tex;
    }
}