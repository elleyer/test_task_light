﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Handlers
{
    public class UIHandler : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private TextMeshProUGUI _roundText;
        
        [SerializeField] private GameObject _loadingTextObj;

        [SerializeField] private Image _background;

        public void SetGameState(bool playable)
        {
            _background.gameObject.SetActive(playable);
            _loadingTextObj.SetActive(!playable);
        }

        public void SetScore(int score)
        {
            _scoreText.text = $"Score: {score}";
        }
        
        public void SetRound(int round)
        {
            _roundText.text = $"Round: {round}";
        }
    }
}