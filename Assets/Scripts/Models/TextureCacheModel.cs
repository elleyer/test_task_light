﻿namespace Models
{
    public class TextureCacheModel
    {
        public string Hash { get; set; }
        public string LocalPath { get; set; }
    }
}