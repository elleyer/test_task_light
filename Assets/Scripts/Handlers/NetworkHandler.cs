using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace Handlers
{
    public class NetworkHandler : MonoBehaviour
    {
        public Action<List<Texture2D>> OnTextureLoadComplete;

        public void LoadTextures(string link) => StartCoroutine(RequestJsonData(link));
        
        //Request json data with some links inside
        private IEnumerator RequestJsonData(string link)
        {
            var request = UnityWebRequest.Get(link);
            var op = request.SendWebRequest();
            yield return op;

            var data = request.downloadHandler.text;
            StartCoroutine(GetTextures(data));
        }

        //Request textures from web
        private IEnumerator GetTextures(string data)
        {
            var textures = new List<Texture2D>();
            var models = JsonConvert.DeserializeObject<ImagesRoot>(data, Settings.JsonSerializerSettings);

            //We can cache everything up so we don't need to download data each time we open our game
            var storage = new StorageContainer<TextureCacheModel>("texturesData.json");
            var localCache = storage.Data;

            //We can send each request in our first foreach loop, then check for result in another to speed up this process
            var operations = new List<UnityWebRequestAsyncOperation>();

            foreach (var model in models.Images)
            {
                UnityWebRequest request;
                
                //Check if we have these textures cached locally
                if (localCache != null)
                {
                    localCache.TryGetValue(model.Link, out var cacheValue);
                    if (cacheValue != null)
                    {
                        //Check if hashes in our database and json file are equal
                        if (string.Equals(model.Hash, cacheValue.Hash))
                        {
                            request = UnityWebRequestTexture.GetTexture(new Uri(Path.Combine(
                                Application.persistentDataPath,
                                cacheValue.LocalPath)).AbsoluteUri);

                            operations.Add(request.SendWebRequest());
                            continue;
                        }
                    }

                    else
                    {
                        //Generate GUID and set it as file's name
                        var guid = Guid.NewGuid().ToString();
                        storage.AppendData(model.Link, new TextureCacheModel
                        {
                            Hash = model.Hash,
                            LocalPath = guid
                        });

                        //Create our new file
                        var stream = File.Create(Path.Combine(Application.persistentDataPath, guid));
                        stream.Dispose();
                    }
                }

                request = UnityWebRequestTexture.GetTexture(model.Link);
                operations.Add(request.SendWebRequest());
            }

            foreach (var op in operations)
            {
                yield return op;
                var texture = DownloadHandlerTexture.GetContent(op.webRequest);

                if (localCache != null && localCache.ContainsKey(op.webRequest.url))
                {
                    //Check if we already have file from this link in our storage. If we don't - we need to store
                    //our texture there
                    var path = Path.Combine(Application.persistentDataPath,
                        localCache[op.webRequest.url].LocalPath);

                    File.WriteAllBytes(path, texture.EncodeToPNG());
                }

                textures.Add(texture);
            }
            
            OnTextureLoadComplete(textures);
        }
    }
}