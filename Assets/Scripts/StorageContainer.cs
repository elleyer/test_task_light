﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public class StorageContainer<T>
{
    public Dictionary<string, T> Data;
    public string Filename { get; private set; }
    public string FullPath { get; private set; }

    public StorageContainer(string filename)
    {
        Filename = filename;
        FullPath = Path.Combine(Application.persistentDataPath, Filename);
            
        var file = File.Open(FullPath, FileMode.OpenOrCreate);
        file.Dispose();

        Data = JsonConvert.DeserializeObject<Dictionary<string, T>>(File.ReadAllText(FullPath)) 
               ?? new Dictionary<string, T>();
    }

    public void AppendData(string key, T value)
    {
        Data.Add(key, value);
        var serialized = JsonConvert.SerializeObject(Data, Formatting.Indented, Settings.JsonSerializerSettings);

        File.WriteAllText(FullPath, serialized);
    }

    public void RemoveData(string key)
    {
        Data.Remove(key);
    }
}