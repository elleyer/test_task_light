﻿using System.Collections.Generic;
using Newtonsoft.Json;

public class ImagesRoot
{
    public List<ImageModel> Images { get; set; }
}

public class ImageModel
{
    public string Link { get; set; }
    public string Hash { get; set; }
}